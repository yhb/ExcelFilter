package com.yhb.zll;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

public class PinyinUtils {

	 public static String getFullSpell(String chinese) {   
         StringBuffer pybf = new StringBuffer();   
         char[] arr = chinese.toCharArray();   
         HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();   
         defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);   
         defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);   
         for (int i = 0; i < arr.length; i++) {   
                 if (arr[i] > 128) {   
                         try {   
                                 pybf.append(PinyinHelper.toHanyuPinyinStringArray(arr[i], defaultFormat)[0]);   
                         } catch (BadHanyuPinyinOutputFormatCombination e) {   
                                 e.printStackTrace();   
                         }   
                 } else {   
                         pybf.append(arr[i]);   
                 }   
         }   
         return pybf.toString();   
 }  
}
