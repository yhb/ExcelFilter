package com.yhb.zll;

import java.util.regex.Pattern;

public class MatchUtils {

	public static boolean containsPhone(String str) {
		if (str == null) return false;
		Pattern p = Pattern.compile("\\w*1\\d{10}\\w*");
		
		return p.matcher(str).matches();
	}
	
	public static boolean containsQQ(String name, String mail) {
		if (mail == null || name == null) return false;
		String[] mailParts = mail.split("@");
		if(mailParts.length != 2) {
			return false;
		}
		String mailName = mailParts[0];
		String mailServer = mailParts[1];
		if(mailServer.equals("qq.com") && name.contains(mailName)) {
			return true;
		}
		return false;
	}
	
	public static boolean containsName(String userName, String name) {
		if(userName == null || name == null) return false;
		String pinyin = PinyinUtils.getFullSpell(name);
		if(userName.contains(pinyin)) {
			return true;
		}
		return false;
	}
}
