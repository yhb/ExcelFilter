package com.yhb.zll;

import java.io.File;
import java.io.IOException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFeatures;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


public class ExcelHelper {
	
	public void run() {
		Workbook wb = openExcel();
		Sheet sheet = getSheet(wb);
		int row = sheet.getRows();
		int column = sheet.getColumns();
		System.out.println(row + "行" + column + "列");
		Cell[] cells = sheet.getColumn(4);
		for (Cell cell :cells) {
			String username = cell.getContents();
			int currentRow = cell.getRow();
			//System.out.println("第" + currentRow + "行 " + username);
			Cell mailCell = sheet.getCell(6,currentRow);
			Cell nameCell = sheet.getCell(3, currentRow);
			String mail = mailCell.getContents();
			String realName = nameCell.getContents();
			//System.out.println("mail = " + mail);
			//System.out.println(username);
			if(MatchUtils.containsPhone(username)) {
				//System.out.println(username + "包含手机号");
			} else if(MatchUtils.containsQQ(username, mail)) {
				//System.out.println(username + "包含QQ");
			} else if(MatchUtils.containsName(username, realName)) {
				System.out.println(username + "包含" + realName);
			}
		}
	}
	
	private Workbook openExcel() {
		Workbook workbook = null;
		try {
			workbook = Workbook.getWorkbook(new File("lcd.xls"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return workbook;
	}

	private Sheet getSheet(Workbook workbook) {
		return workbook.getSheet(0);
	}
	
	public void run2() throws IOException, RowsExceededException, WriteException {
		WritableWorkbook workbook = Workbook.createWorkbook(new File("lcd.xls"));
		WritableSheet sheet = workbook.getSheet(0);
		int row = sheet.getRows();
		int column = sheet.getColumns();
		System.out.println(row + "行" + column + "列");
		Cell[] cells = sheet.getColumn(4);
		for (Cell cell :cells) {
			String username = cell.getContents();
			int currentRow = cell.getRow();
			//System.out.println("第" + currentRow + "行 " + username);
			Cell mailCell = sheet.getCell(6,currentRow);
			Cell nameCell = sheet.getCell(3, currentRow);
			String mail = mailCell.getContents();
			String realName = nameCell.getContents();
			//System.out.println("mail = " + mail);
			//System.out.println(username);
			if(MatchUtils.containsPhone(username)) {
				System.out.println(username + "包含手机号");
				Label label = new Label(column, currentRow, "手机号");
				sheet.addCell(label);
				workbook.write();
				
			} else if(MatchUtils.containsQQ(username, mail)) {
				System.out.println(username + "包含QQ");
				Label label = new Label(column, currentRow, "QQ");
				sheet.addCell(label);
				workbook.write();
				
			} else if(MatchUtils.containsName(username, realName)) {
				System.out.println(username + "包含" + realName);
				Label label = new Label(column, currentRow, "QQ");
				sheet.addCell(label);
				workbook.write();
			}
		}
		workbook.close();
		workbook = null;
	}
	
	
	

}
